import datetime
import os

from flask import Flask, render_template, redirect, url_for
from forms import HospitalForm

from models import Hosps
from database import db_session

app = Flask(__name__)
app.secret_key = os.environ['APP_SECRET_KEY']

@app.route("/", methods=('GET', 'POST'))
def hosp():
    form = HospitalForm()
    if form.validate_on_submit():
        hosp = Hosps(h_name=form.h_name.data, h_address=form.h_address.data, h_city=form.h_city.data, h_state=form.h_state.data, h_zipcode=form.h_zipcode.data, h_county=form.h_county.data, h_phone=form.h_phone.data, h_type=form.h_type.data, h_owner=form.h_owner.data, h_er=form.h_er.data)
        db_session.add(hosp)
        db_session.commit()
        return redirect(url_for('success'))
    return render_template('hosp.html', form=form)

@app.route("/success")
def success():
    return "Hospital successfully added"

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5090, debug=True)
