from database import Base
from sqlalchemy import Column, Integer, String
from sqlalchemy.types import DateTime, Boolean

class Hosps(Base):
    """
    Example Hosps table
    """
    __tablename__ = 'hospital_info'
    h_id = Column(Integer, primary_key=True)
    h_name = Column(String(256))
    h_address = Column(String(256))
    h_city = Column(String(256))
    h_state = Column(String(256))
    h_zipcode = Column(String(256))
    h_country = Column(String(256))
    h_phone = Column(String(256))
    h_type = Column(String(256))
    h_owner = Column(String(256))
    h_er = Column(Boolean())
