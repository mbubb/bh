from flask_wtf import FlaskForm
from wtforms import StringField, BooleanField, validators


class HospitalForm(FlaskForm):
    h_name = StringField('name', [validators.length(max=80)])
    h_address = StringField('address', [validators.length(max=80)])
    h_city = StringField('city', [validators.length(max=60)])
    h_state = StringField('state', [validators.length(max=3)])
    h_zipcode = StringField('zip', [validators.length(min=5, max=10)])
    h_county = StringField('county', [validators.length(max=60)])
    h_phone = StringField('phone', [validators.length(min=10, max=15)])
    h_type = StringField('hospital type', [validators.length(max=80)])
    h_owner = StringField('hospital ownership', [validators.length(max=80)])
    h_er = BooleanField('hospital has emergency services.', [validators.required()])
