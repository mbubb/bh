#!/bin/bash
app="bhapi"
docker build -t ${app} .
docker run -d -p 5090:5090 \
  --name=${app} \
  -v $PWD:/opt/${app} ${app}
