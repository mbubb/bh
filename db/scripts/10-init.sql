drop database if exists hospital_info;
create database hospital_info;
\c hospital_info

create table hospitals (hospitalid serial NOT NULL,
              hospitalname varchar(80),
              address varchar(80),
              city varchar(60),
              state varchar(3),
              zipcode varchar(10),
              county varchar(60),
              phonenumber varchar(15),
              hospitaltype varchar(80),
              ownership varchar(80),
              emergency bool default 'n',
              primary key(hospitalid));
