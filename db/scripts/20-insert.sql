\c hospital_info

insert into hospitals values(DEFAULT, 'SOUTHEAST ALABAMA MEDICAL CENTER',	'1108 ROSS CLARK CIRCLE',	'DOTHAN',	'AL',	'36301',	'HOUSTON',	'3347938701',	'Acute Care Hospitals',	'Government',	'y');
insert into hospitals values(DEFAULT, 'MARSHALL MEDICAL CENTER SOUTH',	'2505 U S HIGHWAY 431 NORTH',	'BOAZ',	'AL',	'35957',	'MARSHALL',	'2565938310',	'Acute Care Hospitals',	'Government',	'y');
insert into hospitals values(DEFAULT, 'ELIZA COFFEE MEMORIAL HOSPITAL',	'205 MARENGO STREET',	'FLORENCE',	'AL',	'35631',	'LAUDERDALE',	'2567688400',	'Acute Care Hospitals',	'Government',	'y');
insert into hospitals values(DEFAULT, 'MIZELL MEMORIAL HOSPITAL',	'702 N MAIN ST',	'OPP',	'AL',	'36467',	'COVINGTON',	'3344933541',	'Acute Care Hospitals',	'Voluntary non-profit - Private',	'y');
insert into hospitals values(DEFAULT, 'CRENSHAW COMMUNITY HOSPITAL',	'101 HOSPITAL CIRCLE',	'LUVERNE',	'AL',	'36049',	'CRENSHAW',	'3343353374',	'Acute Care Hospitals',	'Proprietary',	'y');
insert into hospitals values(DEFAULT, 'HARTSELLE MEDICAL CENTER',	'201 PINE STREET NORTHWEST',	'HARTSELLE',	'AL',	'35640',	'MORGAN',	'2567736511',	'Acute Care Hospitals',	'Proprietary',	'n');
insert into hospitals values(DEFAULT, 'MARSHALL MEDICAL CENTER NORTH',	'8000 ALABAMA HIGHWAY 69',	'GUNTERSVILLE',	'AL',	'35976',	'MARSHALL',	'2565718000',	'Acute Care Hospitals',	'Government',	'y');
insert into hospitals values(DEFAULT, 'ST VINCENTS EAST',	'50 MEDICAL PARK EAST DRIVE',	'BIRMINGHAM',	'AL',	'35235',	'JEFFERSON',	'2058383122',	'Acute Care Hospitals',	'Voluntary non-profit - Private',	'y');
insert into hospitals values(DEFAULT, 'DEKALB REGIONAL MEDICAL CENTER',	'200 MED CENTER DRIVE',	'FORT PAYNE',	'AL',	'35968',	'DE KALB',	'2568453150',	'Acute Care Hospitals',	'Proprietary',	'y');
insert into hospitals values(DEFAULT, 'SOUTHWEST ALABAMA MEDICAL CENTER',	'33700 HIGHWAY 43',	'THOMASVILLE',	'AL',	'36784',	'CLARKE',	'3346366221',	'Acute Care Hospitals',	'Proprietary',	'n');
insert into hospitals values(DEFAULT, 'SHELBY BAPTIST MEDICAL CENTER',	'1000 FIRST STREET NORTH',	'ALABASTER',	'AL',	'35007',	'SHELBY',	'2056208100',	'Acute Care Hospitals',	'Voluntary non-profit - Church',	'y');
insert into hospitals values(DEFAULT, 'CALLAHAN EYE FOUNDATION HOSPITAL',	'1720 UNIVERSITY BLVD',	'BIRMINGHAM',	'AL',	'35233',	'JEFFERSON',	'2053258100',	'Acute Care Hospitals',	'Voluntary non-profit - Private',	'y');
insert into hospitals values(DEFAULT, 'HELEN KELLER MEMORIAL HOSPITAL',	'1300 SOUTH MONTGOMERY AVENUE',	'SHEFFIELD',	'AL',	'35660',	'COLBERT',	'2563864556',	'Acute Care Hospitals',	'Government',	'y');
insert into hospitals values(DEFAULT, 'DALE MEDICAL CENTER',	'126 HOSPITAL AVE',	'OZARK',	'AL',	'36360',	'DALE',	'3347742601',	'Acute Care Hospitals',	'Government',	'y');
