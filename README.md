## BH project
- *Michael Bubb, 26 Feb 2021*

### Postgres & Flask API

Local development setup for an api with a postgres backend and flask api (with an nginx proxy in front). A Docker compose file is present to create the local infrastructure and run the app for testing.

#### simple instructions

After you have "git pulled" the code; cd into the directory. Make any changes to the env file or other config as desired and run:

```
docker-compose up -d

```

In a separate tmux/ screen session tail the logs:

```
docker-compose logs -f

```

To further inspect a running container - docker exec is useful. ```docker ps``` can verify the container name. Run something like:

```
docker exec -it <container_name> bash
```

And you can run commands as needed.

If you need to come back to a "clean slate", make sure no containers are running and:

```
docker kill
docker system prune -af

```


There are many more useful docker debug commands, like ```docker network ls``` but that is beyond this context - https://docs.docker.com/ - has good sections on debugging and productionizing an app.


---

### Description

The docker-compose file delineates 3 services: db, web and api.

#### db

Basic postgres docker image with relevant initialization and data insert scripts that are run when the container is started up. When adding/ changing these files remember that the files will be run in descending order. The 3rd script (777-insert.sql) has a larger database for future use in more extensive testing (especially queries).

The api  will include a webform to update the db. Another (better) way to do this is via api call with json payload.

#### web

The goal is as simple a proxy as possible. When deployed to the cloud this very well will be replaced but an ALB, or api-gateway setup or k8 cluster so this is primarily to access the api across http for local testing.


#### api

Use any programming language that you’re most comfortable with

Define one GET endpoint to return a list of hospitals from the database. For simplicity, you can return only the hospital name but feel free to return any other attributes.

---

### Testing


*Integration tests*

Written in Python using the "requests" library to make calls against the api and verrify access to the db. CI/CD pipeline will call these from the **test** directory.

```
 python3 -m pytest ./test/simple.py

```

---

### notes on prod deploy in cloud

This setup is intended for local development only. To make this production ready a few topics:

#### networking

Docker compose defines simple adhoc network segments (bh_db, bh_web). The docker-compose shows how the nginx proxy layer is nt directly in access of the db. This separation would be enforced in a cloud env by enforcing security groups on subnets.

#### secrets

The env_file makes secrets availible for local dev. This would be replaced by kubernetes secrets (or perhaps Hashicorp Vault).

Instead of accessing env like this:

```
user = os.environ['POSTGRES_USER']
pwd = os.environ['POSTGRES_PASSWORD']
db = os.environ['POSTGRES_DB']
```

To do this locally in such a way to get closer to prod, encrypt a token BASE64:

```
sec_token = $(echo -n <sec> | base64)
```

Create a sec.yaml which includes:

```
apiVersion: v1
kind: Secret
metadata:
  name: flask-app-secrets
type: Opaque
data:
  secret_token: ${sec_token}
```

Apply the k8 secret

```
kubectl apply -f sec.yaml
```

Now you can access it via environment variable in your deployment configuration like the following:

```
apiVersion: apps/v1beta2

...

      env:
        - name: DEBUG_MODE
          value: "1"
        - name: SEC_TOKEN
          valueFrom:
            secretKeyRef:
              name: flask-app-secrets
              key: secret_token
```

#### web layer

Probably better to use something like Gunicorn - tighter with the application.

#### db layer

A service like Aurora as an alternative to a locally run db.

#### docker image size

As the dev gets more stable using smaller images like Alpine will make this more efficient.
