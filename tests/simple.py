import requests

def test_get_success_check_status_code_equals_200():
     response = requests.get("http://localhost:8080/success")
     assert response.status_code == 200
